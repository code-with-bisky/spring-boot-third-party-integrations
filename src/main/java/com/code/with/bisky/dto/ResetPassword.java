package com.code.with.bisky.dto;

public record ResetPassword(String password) {
}
